#ifndef __LIB_HPP__
#define __LIB_HPP__

#include "MockableObj.hpp"

class TestObj
{
    public:
        TestObj() { inst = MockableObj::create(); }
        virtual ~TestObj() = default;
        int some_function();

    private:
        std::shared_ptr<MockableObj> inst; 
};

#endif // __LIB_HPP__