#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <memory>

#include "MockableObj.hpp"
#include "TestObj.hpp"

class MockObj : public MockableObj {

public:
  MockObj(){};
  ~MockObj(){};

  MOCK_METHOD(int, foo, (),(override));
  MOCK_METHOD(int, bar, (), (override));
};

static std::weak_ptr<MockObj> mock_intf;
std::shared_ptr<MockableObj> MockableObj::create() {
  auto m = std::make_shared<MockObj>();
  mock_intf = m;
  return m;
}

TEST(test_lib, some_function) {
  /* Arrange */
  TestObj obj;

  /* Expects */
  auto m = mock_intf.lock();
  EXPECT_CALL(*m, foo()).Times(1);
  EXPECT_CALL(*m, bar()).Times(1);
  m.reset();

  /* Act */
  obj.some_function();
}