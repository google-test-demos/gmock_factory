# Objective
Demo on testing class with mocking object which created by testee.

# Steps
1. prepare enviroment with googletest

```
docker run --rm -v$(pwd):/code -it srzzumix/googletest bash 
```

2. build binaries

```
cmake -B build
cd build
make
```

3. execution
```
# Demo program with real object
./main
# mocking test
./gmock_facotry
```
