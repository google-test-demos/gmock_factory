#ifndef __INTERFACE_HPP__
#define __INTERFACE_HPP__
#include <memory>

class MockableObj 
{
public:
    virtual int foo()=0;
    virtual int bar()=0;
    static std::shared_ptr<MockableObj> create();
    virtual ~MockableObj() = default;
    MockableObj(){};
};

#endif // __INTERFACE_HPP__
