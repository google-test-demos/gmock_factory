#include "MockableObj.hpp"

class MockableObj_Impl : public MockableObj {
public:
    MockableObj_Impl() { };
    ~MockableObj_Impl() { };
    int foo();
    int bar();
};

std::shared_ptr<MockableObj> MockableObj::create()
{
    return std::make_shared<MockableObj_Impl>();
}


int MockableObj_Impl::foo()
{
    return 2;
}

int MockableObj_Impl::bar()
{
    return 2;
}
