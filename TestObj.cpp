#include "TestObj.hpp"
   
int TestObj::some_function()
{
    int foo_ret = inst->foo();
    int bar_ret = inst->bar();

    return foo_ret+bar_ret;
}